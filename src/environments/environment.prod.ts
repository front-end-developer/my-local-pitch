export const environment = {
  production: true,
  API_ENDPOINT: {
    PITCHES: 'https://api-v2.mylp.info/pitches',
    SPORTS: 'https://api-v2.mylp.info/sports'
  },
};
