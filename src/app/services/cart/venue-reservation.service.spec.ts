import { TestBed, inject } from '@angular/core/testing';

import { VenueReservationService } from './venue-reservation.service';

describe('VenueReservationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VenueReservationService]
    });
  });

  it('should be created', inject([VenueReservationService], (service: VenueReservationService) => {
    expect(service).toBeTruthy();
  }));
});
