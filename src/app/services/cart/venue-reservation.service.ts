import { Injectable } from '@angular/core';
import {VenueReservation} from "../../modules/common/cart/models/venue-reservation";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class VenueReservationService {

  private venueReservationObserver = new BehaviorSubject<VenueReservation | null>(null);
  public venueReservationChanges$ = this.venueReservationObserver.asObservable();

  constructor() { }

  public broadcastVenueReservationChangeNotification(venueInCart: VenueReservation): void {
    this.venueReservationObserver.next(venueInCart);
  }
}
