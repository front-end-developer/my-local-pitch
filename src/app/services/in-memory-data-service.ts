/**
 * Created by Mark Webley on 16/04/2019.
 */
import { InMemoryDbService} from 'angular-in-memory-web-api';
import { pitches } from './stubs/all-pitches';
import { venueAvailability } from './stubs/pitch-availability';
import {Injectable} from "@angular/core";
import {venueInformation} from "./stubs/venueInformation";

@Injectable()
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    // pitches()
    const pitches = [
      {
        "type": "pitches",
        "id": 33192,
        "attributes": {
          "name": "Drayton Green Running Track",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1484732741_189734.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1484732741_189734.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1484732741_189734.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "grass",
          "facilities": [],
          "payment": [
            {
              "key": "Free",
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "Drayton Green Running Track, which is based in Ealing, has Athletics facilities. This facility is membership only.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "21342"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1180"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33192",
          "weblink": "/london/venue/drayton-green-running-track/athletics-outdoor-33192"
        }
      },
      {
        "type": "pitches",
        "id": 33207,
        "attributes": {
          "name": "Perivale Park",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1398146047_295396.gif"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1398146047_295396.gif"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1398146047_295396.gif"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "grass",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "Perivale Park, which is based in Ealing, has Athletics facilities. This facility is pay as you play.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20730"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1194"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33207",
          "weblink": "/london/venue/perivale-park/athletics-outdoor-33207"
        }
      },
      {
        "type": "pitches",
        "id": 33218,
        "attributes": {
          "name": "The Japanese School Track",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1418045969_384700.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1418045969_384700.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1418045969_384700.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "artificial",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "The Japanese School Track offers athletics on a 160m track of artificial surface. It is located in Ealing, West London and the nearest London tube is West Acton. The track is available to be hired on a pay as you play basis and it has changing rooms & free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "21349"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1204"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33218",
          "weblink": "/london/venue/the-japanese-school-track/athletics-outdoor-33218"
        }
      },
      {
        "type": "pitches",
        "id": 33222,
        "attributes": {
          "name": "Willesden Sports Centre",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1486549535_234774.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1486549535_234774.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1486549535_234774.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "synthetic rubber",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "Willesden Sports Centre offers athletics on a 400m track of synthetic rubber surface. It is located in Brent, North London and the nearest London tube is Willesden Junction. The track is available to be hired on a pay as you play basis and it has changing rooms, flood lights, free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20911"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1208"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33222",
          "weblink": "/london/venue/willesden-sports-centre/athletics-outdoor-33222"
        }
      },
      {
        "type": "pitches",
        "id": 33200,
        "attributes": {
          "name": "Linford Christie Athletics Track",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1397658521_669128.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1397658521_669128.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1397658521_669128.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "synthetic rubber",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "*This facility must be booked in advance of use*\r\n\r\nLinford Christie Athletics Track offers athletics on a 400m track of synthetic rubber surface. It is located in Hammersmith & Fulham, West London and the nearest London tube is East Acton. The track is available to be hired on a pay as you play basis and it has changing rooms, flood lights, free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20709"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1187"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33200",
          "weblink": "/london/venue/linford-christie-athletics-track/athletics-outdoor-33200"
        }
      },
      {
        "type": "pitches",
        "id": 33210,
        "attributes": {
          "name": "Ravenscourt Park",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1395919048_632154.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1395919048_632154.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1395919048_632154.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "grass",
          "facilities": [],
          "payment": [
            {
              "key": "Free",
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "*This facility must be booked in advance of use*\r\n\r\nRavenscourt Park offers athletics on a 60m track of grass surface. It is located in Hammersmith & Fulham, West London and the nearest London tube is Ravenscourt Park. The track does not have any hiring charges and it has changing rooms, flood lights, free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20734"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1197"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33210",
          "weblink": "/london/venue/ravenscourt-park/athletics-outdoor-33210"
        }
      },
      {
        "type": "pitches",
        "id": 33551,
        "attributes": {
          "name": "Osterley Sports and Athletics Centre",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1408601408_448053.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1408601408_448053.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1408601408_448053.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "synthetic rubber",
          "facilities": [
            "floodlights",
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": "\u00a34.35 / hour",
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": null,
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "21390"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1530"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33551",
          "weblink": "/london/venue/osterley-sports-and-athletics-centre/athletics-outdoor-33551"
        }
      }
    ];
    const pitchAvailability = venueAvailability();
    const pitchInformation = venueInformation();
    return {
      pitches: pitches,
      venueAvailability: pitchAvailability,
      venueInformation: pitchInformation
    };
  }

}
