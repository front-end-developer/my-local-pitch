import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {environment} from "../../environments/environment";
import {catchError, tap} from "rxjs/operators";
import {pitches} from './stubs/all-pitches';
import {of} from "rxjs/observable/of";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/do";
import {Subject} from "rxjs/Subject";
import {PitchesModel} from "../modules/list-container/model/pitches-model";

@Injectable()
export class PitchService {

  private listAllPitchesObserver = new Subject<PitchesModel | null>();
  private errorMessageObserver = new Subject<string | null>();
  public errorMessageChanges$ = this.errorMessageObserver.asObservable();
  public listAllPitchesChanges$ = this.listAllPitchesObserver.asObservable();
  private listAllPitches: any;

  constructor(private http: HttpClient) {}

  /**
   * @description   Get all pitches
   * @returns       {Observable<any>}
   */
  getAllPitches(selectedSport: string = null, keywords: string = null): Observable<any> {
    let queryDetails = '';
    if (selectedSport !== null || keywords !== null) {
      queryDetails = `?filter[city]=${keywords}&filter[sport]=${selectedSport}&filter[radius]=5mi&include=venues&page[number]=1&page[size]=20`;
    }
    return this.http.get<any>(`${environment.API_ENDPOINT.PITCHES}${queryDetails}`).pipe(
      tap(
        data => {
          this.listAllPitches = data;
        }
      ),
      catchError(this.handleError<any>())
    )
  }

  /**
   * @description   Get pitches information by id
   * @returns       {Observable<any>}
   */
  getInformation(pitchId: number): Observable<any> {
    console.log('GET url', `${environment.API_ENDPOINT.PITCHES}/${pitchId}?include=venues`);
    return this.http.get<any>(`${environment.API_ENDPOINT.PITCHES}/${pitchId}?include=venues`).pipe(
      tap(
        data => {
          console.log('show venue information', data);
        }
      ),
      catchError(this.handleError<any>())
    )
  }

  /**
   * @description   Get pitches slots & availability by id
   * @returns       {Observable<any>}
   * TODO:          insert into dynamic calender
   */
  getAvailability(pitchId: number, startDate: string = null, endDate: string = null): Observable<any> {
    let starts = '', ends = '', startDateValue = new Date(startDate);
    if (endDate !== null) {
      let endDateValue = new Date(endDate);
      if (String(endDateValue) === 'Invalid Date') {
        console.warn('Invalid date formate for: ', endDate);
        return;
      }
      ends = `filter[ends]=${endDate}`;
    }

    if (String(startDateValue) === 'Invalid Date' || startDateValue === null) {
      console.warn('Invalid date formate for: ', startDate);
      return;
    }
    starts = `filter[starts]=${startDate}`;

    if (endDate !== null && new Date(endDate) < new Date(startDate)) {
      console.warn('End date must be greater then start date: ', endDate);
      return;
    }

    console.log('GET url', `${environment.API_ENDPOINT.PITCHES}/${pitchId}/slots?filter[starts]=2018-01-09`);
    return this.http.get<any>(`${environment.API_ENDPOINT.PITCHES}/${pitchId}/slots?filter[starts]=2018-01-09`).pipe(
      tap(
        data => {
          console.log('show availability', data);
        }
      ),
      catchError(this.handleError<any>())
    )
  }

  public getSports() {
    return this.http.get<any>(`${environment.API_ENDPOINT.SPORTS}`).pipe(
      tap(
        data => {
          console.log('list sports', data);
        }
      ),
      catchError(this.handleError<any>())
    )
  }

  public broadcastAllPitchChangeNotification() {
    this.listAllPitchesObserver.next(this.listAllPitches);
  }

  public broadcastErrorNotification(message: string = '') {
    this.errorMessageObserver.next(message);
  }

  private handleError<T> (result?: T) {
    return (error: any): Observable<T | any> => {
      console.error(error);
      if (error.error.errors.length && typeof error.error.errors[0].title !== 'undefined') {
        console.log('error message: ', error.error.errors[0].title);
        this.broadcastErrorNotification(error.error.errors[0].title);
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
