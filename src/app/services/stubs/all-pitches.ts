/**
 * Created by Mark Webley on 16/04/2019.
 *
 * api example:
 * https://api-v2.mylocalpitch.com/pitches?filter[location]=51.550501,%20-0.3048409&filter[sport]=athletics&filter[radius]=5mi&include=venues&page[number]=1&page[size]=20
 */
export function pitches() {
  return {
    "meta": {
      "total_pages": 1,
      "total_items": 7,
      "on_page_online_venues": 0,
      "total_online_venues": 0,
      "aggregations": {
        "formats": [
          {
            "key": "outdoor",
            "doc_count": 53
          },
          {
            "key": "indoor",
            "doc_count": 5
          },
          {
            "key": "running track",
            "doc_count": 1
          }
        ],
        "surfaces": [
          {
            "key": "synthetic rubber",
            "doc_count": 23
          },
          {
            "key": "artificial",
            "doc_count": 15
          },
          {
            "key": "grass",
            "doc_count": 13
          },
          {
            "key": "hard (macadam)",
            "doc_count": 3
          },
          {
            "key": "indoor",
            "doc_count": 3
          },
          {
            "key": "n/a",
            "doc_count": 2
          }
        ],
        "available_slots": [],
        "is_partner": [
          {
            "key": 0,
            "key_as_string": "false",
            "doc_count": 52
          },
          {
            "key": 1,
            "key_as_string": "true",
            "doc_count": 7
          }
        ],
        "is_online": [
          {
            "key": 0,
            "key_as_string": "false",
            "doc_count": 11
          }
        ],
        "facilities": [
          {
            "key": "changing rooms",
            "doc_count": 42
          },
          {
            "key": "free parking",
            "doc_count": 24
          },
          {
            "key": "floodlights",
            "doc_count": 10
          }
        ],
        "radius": [
          {
            "key": "within 1 km",
            "from": 0,
            "to": 1,
            "doc_count": 0
          },
          {
            "key": "within 3 km",
            "from": 0,
            "to": 3,
            "doc_count": 0
          },
          {
            "key": "within 5 km",
            "from": 0,
            "to": 5,
            "doc_count": 3
          },
          {
            "key": "within 10 km",
            "from": 0,
            "to": 10,
            "doc_count": 9
          },
          {
            "key": "within 1 mile",
            "from": 0,
            "to": 1.60934,
            "doc_count": 0
          },
          {
            "key": "within 3 miles",
            "from": 0,
            "to": 4.82803,
            "doc_count": 3
          },
          {
            "key": "within 5 miles",
            "from": 0,
            "to": 8.0467200000000005,
            "doc_count": 7
          },
          {
            "key": "within 10 miles",
            "from": 0,
            "to": 16.093399999999999,
            "doc_count": 16
          }
        ]
      },
      "available_filters": {
        "sport": "e.g. 'football', 'rowing', ...",
        "city": "e.g. 'london', 'dublin' (defaults to london)",
        "format": "e.g. '5 a side', '11 a side', 'Indoor, Outdoor'",
        "location": "e.g. '51.503407,-0.127592' or '10 Downing Street, london'",
        "starts": "e.g. '2019-04-15 21:50'",
        "ends": "e.g. '2019-04-15 21:50'"
      }
    },
    "links": {
      "first": "/pitches?filter[location]=51.550501,-0.3048409&filter[sport]=athletics&filter[radius]=5mi&include=venues&page[number]=1&page[size]=20",
      "self": "/pitches?filter[location]=51.550501,-0.3048409&filter[sport]=athletics&filter[radius]=5mi&include=venues&page[number]=1&page[size]=20"
    },
    "data": [
      {
        "type": "pitches",
        "id": "33192",
        "attributes": {
          "name": "Drayton Green Running Track",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1484732741_189734.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1484732741_189734.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1484732741_189734.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "grass",
          "facilities": [],
          "payment": [
            {
              "key": "Free",
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "Drayton Green Running Track, which is based in Ealing, has Athletics facilities. This facility is membership only.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "21342"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1180"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33192",
          "weblink": "/london/venue/drayton-green-running-track/athletics-outdoor-33192"
        }
      },
      {
        "type": "pitches",
        "id": "33207",
        "attributes": {
          "name": "Perivale Park",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1398146047_295396.gif"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1398146047_295396.gif"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1398146047_295396.gif"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "grass",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "Perivale Park, which is based in Ealing, has Athletics facilities. This facility is pay as you play.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20730"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1194"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33207",
          "weblink": "/london/venue/perivale-park/athletics-outdoor-33207"
        }
      },
      {
        "type": "pitches",
        "id": "33218",
        "attributes": {
          "name": "The Japanese School Track",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1418045969_384700.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1418045969_384700.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1418045969_384700.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "artificial",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "The Japanese School Track offers athletics on a 160m track of artificial surface. It is located in Ealing, West London and the nearest London tube is West Acton. The track is available to be hired on a pay as you play basis and it has changing rooms & free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "21349"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1204"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33218",
          "weblink": "/london/venue/the-japanese-school-track/athletics-outdoor-33218"
        }
      },
      {
        "type": "pitches",
        "id": "33222",
        "attributes": {
          "name": "Willesden Sports Centre",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1486549535_234774.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1486549535_234774.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1486549535_234774.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "synthetic rubber",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "Willesden Sports Centre offers athletics on a 400m track of synthetic rubber surface. It is located in Brent, North London and the nearest London tube is Willesden Junction. The track is available to be hired on a pay as you play basis and it has changing rooms, flood lights, free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20911"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1208"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33222",
          "weblink": "/london/venue/willesden-sports-centre/athletics-outdoor-33222"
        }
      },
      {
        "type": "pitches",
        "id": "33200",
        "attributes": {
          "name": "Linford Christie Athletics Track",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1397658521_669128.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1397658521_669128.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1397658521_669128.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "synthetic rubber",
          "facilities": [
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": null,
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "*This facility must be booked in advance of use*\r\n\r\nLinford Christie Athletics Track offers athletics on a 400m track of synthetic rubber surface. It is located in Hammersmith & Fulham, West London and the nearest London tube is East Acton. The track is available to be hired on a pay as you play basis and it has changing rooms, flood lights, free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20709"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1187"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33200",
          "weblink": "/london/venue/linford-christie-athletics-track/athletics-outdoor-33200"
        }
      },
      {
        "type": "pitches",
        "id": "33210",
        "attributes": {
          "name": "Ravenscourt Park",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1395919048_632154.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1395919048_632154.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1395919048_632154.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "grass",
          "facilities": [],
          "payment": [
            {
              "key": "Free",
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": "*This facility must be booked in advance of use*\r\n\r\nRavenscourt Park offers athletics on a 60m track of grass surface. It is located in Hammersmith & Fulham, West London and the nearest London tube is Ravenscourt Park. The track does not have any hiring charges and it has changing rooms, flood lights, free parking facility available.",
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "20734"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1197"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33210",
          "weblink": "/london/venue/ravenscourt-park/athletics-outdoor-33210"
        }
      },
      {
        "type": "pitches",
        "id": "33551",
        "attributes": {
          "name": "Osterley Sports and Athletics Centre",
          "status": "active",
          "images": {
            "small": [
              "https://images.mylocalpitch.com/small/pitch_image_1408601408_448053.jpeg"
            ],
            "medium": [
              "https://images.mylocalpitch.com/medium/pitch_image_1408601408_448053.jpeg"
            ],
            "large": [
              "https://images.mylocalpitch.com/large/pitch_image_1408601408_448053.jpeg"
            ]
          },
          "sport": "athletics",
          "format": "outdoor",
          "surface": "synthetic rubber",
          "facilities": [
            "floodlights",
            "changing rooms",
            "free parking"
          ],
          "payment": [
            {
              "key": "\u00a34.35 / hour",
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
          "about": null,
          "mlp": false,
          "online_booking": false,
          "tac_link": null,
          "operator": null,
          "opt_in_third_parties": false,
          "featured": false,
          "featuredOrder": 0,
          "showFFPopup": false,
          "available_slots": {
            "morning": 0,
            "evening": 0,
            "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
              "id": "21390"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1530"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33551",
          "weblink": "/london/venue/osterley-sports-and-athletics-centre/athletics-outdoor-33551"
        }
      }
    ],
    "included": [
      {
        "type": "venues",
        "id": "21342",
        "attributes": {
          "name": "Drayton Green Running Track",
          "url": "drayton-green-running-track",
          "address1": "Drayton Green Running Track",
          "address2": "Drayton Green, West Ealing",
          "latitude": 51.515096,
          "longitude": -0.32340400000000002,
          "town": "Ealing",
          "nearest_tube": "Drayton Green (train)",
          "postcode": "W13 0LA",
          "area": "West",
          "council": "Ealing",
          "featured": false,
          "contact_number": "-",
          "opening_hours": " ",
          "meta_description": null,
          "meta_title": null,
          "currency": "GBP",
          "parking_type": "pay and display"
        },
        "relationships": {
          "pitches": {
            "data": [
              {
                "type": "pitches",
                "id": "33192"
              }
            ]
          }
        },
        "links": {
          "self": "/venues/21342",
          "similar": "/venues/21342/similar"
        }
      },
      {
        "type": "venues",
        "id": "20730",
        "attributes": {
          "name": "Perivale Park",
          "url": "perivale-park",
          "address1": "Perivale Park",
          "address2": "Leaver Gardens, Greenford",
          "latitude": 51.527140000000003,
          "longitude": -0.35153400000000001,
          "town": "Ealing",
          "nearest_tube": "South Greenford (train)",
          "postcode": "UB6 9BG",
          "area": "West",
          "council": "Ealing",
          "featured": false,
          "contact_number": "0208 575 5776",
          "opening_hours": "08.00 - Dusk",
          "meta_description": null,
          "meta_title": null,
          "currency": "GBP",
          "parking_type": "free parking"
        },
        "relationships": {
          "pitches": {
            "data": [
              {
                "type": "pitches",
                "id": "32369"
              },
              {
                "type": "pitches",
                "id": "32767"
              },
              {
                "type": "pitches",
                "id": "33207"
              },
              {
                "type": "pitches",
                "id": "33488"
              },
              {
                "type": "pitches",
                "id": "33489"
              }
            ]
          }
        },
        "links": {
          "self": "/venues/20730",
          "similar": "/venues/20730/similar"
        }
      },
      {
        "type": "venues",
        "id": "21349",
        "attributes": {
          "name": "The Japanese School Track",
          "url": "the-japanese-school-track",
          "address1": "The Japanese School Track",
          "address2": "The Japanese School, 87 Creffield Road, Acton",
          "latitude": 51.513480999999999,
          "longitude": -0.28139500000000001,
          "town": "Ealing",
          "nearest_tube": "West Acton",
          "postcode": "W3 9PU",
          "area": "West",
          "council": "Ealing",
          "featured": false,
          "contact_number": "0208 993 7145",
          "opening_hours": " ",
          "meta_description": null,
          "meta_title": null,
          "currency": "GBP",
          "parking_type": "free parking"
        },
        "relationships": {
          "pitches": {
            "data": [
              {
                "type": "pitches",
                "id": "33218"
              }
            ]
          }
        },
        "links": {
          "self": "/venues/21349",
          "similar": "/venues/21349/similar"
        }
      },
      {
        "type": "venues",
        "id": "20911",
        "attributes": {
          "name": "Willesden Sports Centre",
          "url": "willesden-sports-centre",
          "address1": "Willesden Sports Centre",
          "address2": "Donnington Road, Willesden",
          "latitude": 51.541403000000003,
          "longitude": -0.231846,
          "town": "Brent",
          "nearest_tube": "Willesden Junction",
          "postcode": "NW10 3QX",
          "area": "North",
          "council": "Brent",
          "featured": false,
          "contact_number": "0208 955 1120",
          "opening_hours": "Weekday: 06.30 - 21.30; Sat: 06.30 - 18.00; Sun: 08.00 - 17.30",
          "meta_description": null,
          "meta_title": null,
          "currency": "GBP",
          "parking_type": "free parking"
        },
        "relationships": {
          "pitches": {
            "data": [
              {
                "type": "pitches",
                "id": "32347"
              },
              {
                "type": "pitches",
                "id": "32509"
              },
              {
                "type": "pitches",
                "id": "32895"
              },
              {
                "type": "pitches",
                "id": "33222"
              },
              {
                "type": "pitches",
                "id": "33386"
              },
              {
                "type": "pitches",
                "id": "34520"
              }
            ]
          }
        },
        "links": {
          "self": "/venues/20911",
          "similar": "/venues/20911/similar"
        }
      },
      {
        "type": "venues",
        "id": "20709",
        "attributes": {
          "name": "Linford Christie Athletics Track",
          "url": "linford-christie-athletics-track",
          "address1": "Linford Christie Athletics Track",
          "address2": "Artillery Way, Du Cane Road, Wormwood Scrubs",
          "latitude": 51.516022,
          "longitude": -0.240483,
          "town": "Hammersmith and Fulham",
          "nearest_tube": "East Acton",
          "postcode": "W12 0AE",
          "area": "West",
          "council": "Hammersmith and Fulham",
          "featured": true,
          "contact_number": "0208 753 4103",
          "opening_hours": "08.00 - 22.00",
          "meta_description": null,
          "meta_title": null,
          "currency": "GBP",
          "parking_type": "free parking"
        },
        "relationships": {
          "pitches": {
            "data": [
              {
                "type": "pitches",
                "id": "32075"
              },
              {
                "type": "pitches",
                "id": "32256"
              },
              {
                "type": "pitches",
                "id": "32362"
              },
              {
                "type": "pitches",
                "id": "33200"
              },
              {
                "type": "pitches",
                "id": "33725"
              }
            ]
          }
        },
        "links": {
          "self": "/venues/20709",
          "similar": "/venues/20709/similar"
        }
      },
      {
        "type": "venues",
        "id": "20734",
        "attributes": {
          "name": "Ravenscourt Park",
          "url": "ravenscourt-park",
          "address1": "Ravenscourt Park",
          "address2": "Kings Street, Fulham",
          "latitude": 51.493853000000001,
          "longitude": -0.23973700000000001,
          "town": "Hammersmith and Fulham",
          "nearest_tube": "Ravenscourt Park",
          "postcode": "W6 0TZ",
          "area": "West",
          "council": "Hammersmith and Fulham",
          "featured": true,
          "contact_number": "0208 753 4103",
          "opening_hours": "09.00 - Dusk",
          "meta_description": null,
          "meta_title": null,
          "currency": "GBP",
          "parking_type": "pay and display"
        },
        "relationships": {
          "pitches": {
            "data": [
              {
                "type": "pitches",
                "id": "32100"
              },
              {
                "type": "pitches",
                "id": "32294"
              },
              {
                "type": "pitches",
                "id": "32784"
              },
              {
                "type": "pitches",
                "id": "32952"
              },
              {
                "type": "pitches",
                "id": "33210"
              }
            ]
          }
        },
        "links": {
          "self": "/venues/20734",
          "similar": "/venues/20734/similar"
        }
      },
      {
        "type": "venues",
        "id": "21390",
        "attributes": {
          "name": "Osterley Sports and Athletics Centre",
          "url": "osterley-sports-and-athletics-centre",
          "address1": "Osterley Sports and Athletics Centre",
          "address2": "120 Wood Lane",
          "latitude": 51.482244999999999,
          "longitude": -0.33718199999999998,
          "town": "Hounslow",
          "nearest_tube": "Osterley",
          "postcode": "TW7 5FF",
          "area": null,
          "council": "Hounslow",
          "featured": false,
          "contact_number": "0845 456 6675",
          "opening_hours": "Weekday: 09.00 - 22.00 Weekend: 09.00 - 18.00",
          "meta_description": null,
          "meta_title": null,
          "currency": "GBP",
          "parking_type": "pay and display"
        },
        "relationships": {
          "pitches": {
            "data": [
              {
                "type": "pitches",
                "id": "33548"
              },
              {
                "type": "pitches",
                "id": "33549"
              },
              {
                "type": "pitches",
                "id": "33550"
              },
              {
                "type": "pitches",
                "id": "33551"
              }
            ]
          }
        },
        "links": {
          "self": "/venues/21390",
          "similar": "/venues/21390/similar"
        }
      }
    ]
  };
}
