/**
 * Created by Mark Webley on 16/04/2019.
 */
export function venueInformation() {
  return {
      "data": {
        "type": "pitches",
          "id": "33192",
          "attributes": {
          "name": "Drayton Green Running Track",
            "status": "active",
            "images": {
            "small": [
              "https://s3.eu-west-2.amazonaws.com/images.mlp.staging/small/pitch_image_1484732741_189734.jpeg"
            ],
              "medium": [
              "https://s3.eu-west-2.amazonaws.com/images.mlp.staging/medium/pitch_image_1484732741_189734.jpeg"
            ],
              "large": [
              "https://s3.eu-west-2.amazonaws.com/images.mlp.staging/large/pitch_image_1484732741_189734.jpeg"
            ]
          },
          "sport": "athletics",
            "format": "outdoor",
            "surface": "grass",
            "facilities": [],
            "payment": [
            {
              "key": "Free",
              "value": null,
              "desc1": null,
              "desc2": null
            }
          ],
            "about": "Drayton Green Running Track, which is based in Ealing, has Athletics facilities. This facility is membership only.",
            "mlp": false,
            "online_booking": false,
            "tac_link": null,
            "operator": null,
            "opt_in_third_parties": false,
            "featured": false,
            "featuredOrder": 0,
            "showFFPopup": false,
            "available_slots": {
            "morning": 0,
              "evening": 0,
              "weekend": 0
          }
        },
        "relationships": {
          "venues": {
            "data": {
              "type": "venues",
                "id": "21342"
            }
          },
          "pitchImages": {
            "data": [
              {
                "type": "pitch-images",
                "id": "1180"
              }
            ]
          }
        },
        "links": {
          "self": "/pitches/33192",
            "weblink": "/london/venue/drayton-green-running-track/athletics-outdoor-33192"
        }
    },
    "included": [
    {
      "type": "venues",
      "id": "21342",
      "attributes": {
        "name": "Drayton Green Running Track",
        "url": "drayton-green-running-track",
        "address1": "Drayton Green Running Track",
        "address2": "Drayton Green, West Ealing",
        "latitude": 51.515096,
        "longitude": -0.32340400000000002,
        "town": "Ealing",
        "nearest_tube": "Drayton Green (train)",
        "postcode": "W13 0LA",
        "area": "West",
        "council": "Ealing",
        "featured": false,
        "contact_number": "-",
        "opening_hours": " ",
        "meta_description": null,
        "meta_title": null,
        "currency": "GBP",
        "parking_type": "pay and display"
      },
      "relationships": {
        "pitches": {
          "data": [
            {
              "type": "pitches",
              "id": "33192"
            }
          ]
        }
      },
      "links": {
        "self": "/venues/21342",
        "similar": "/venues/21342/similar"
      }
    }
  ]
  };
}
