/**
 * Created by Mark Webley on 19/04/2019.
 */
export function getSports() {
  return {
    meta: {
      total_items: 20,
      filter: {
        city: null
      }
    },
    data: [
      {
        type: "sports",
        id: "athletics",
        attributes: {
          name: "Athletics"
        }
      },
      {
        type: "sports",
        id: "badminton",
        attributes: {
          name: "Badminton"
        }
      },
      {
        type: "sports",
        id: "basketball",
        attributes: {
          name: "Basketball"
        }
      },
      {
        type: "sports",
        id: "cricket",
        attributes: {
          name: "Cricket"
        }
      },
      {
        type: "sports",
        id: "esports",
        attributes: {
          name: "Esports"
        }
      },
      {
        type: "sports",
        id: "football",
        attributes: {
          name: "Football"
        }
      },
      {
        type: "sports",
        id: "futsal",
        attributes: {
          name: "Futsal"
        }
      },
      {
        type: "sports",
        id: "gaa",
        attributes: {
          name: "GAA"
        }
      },
      {
        type: "sports",
        id: "golf",
        attributes: {
          name: "Golf"
        }
      },
      {
        type: "sports",
        id: "gym",
        attributes: {
          name: "Gym"
        }
      },
      {
        type: "sports",
        id: "handball",
        attributes: {
          name: "Handball"
        }
      },
      {
        type: "sports",
        id: "hockey",
        attributes: {
          name: "Hockey"
        }
      },
      {
        type: "sports",
        id: "netball",
        attributes: {
          name: "Netball"
        }
      },
      {
        type: "sports",
        id: "rugby",
        attributes: {
          name: "Rugby"
        }
      },
      {
        type: "sports",
        id: "space-hire",
        attributes: {
          name: "Space Hire"
        }
      },
      {
        type: "sports",
        id: "squash",
        attributes: {
          name: "Squash"
        }
      },
      {
        type: "sports",
        id: "swimming",
        attributes: {
          name: "Swimming"
        }
      },
      {
        type: "sports",
        id: "table-tennis",
        attributes: {
          name: "Table Tennis"
        }
      },
      {
        type: "sports",
        id: "tennis",
        attributes: {
          name: "Tennis"
        }
      },
      {
        type: "sports",
        id: "volleyball",
        attributes: {
          name: "Volleyball"
        }
      }
    ]
  };
}
