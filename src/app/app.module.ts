/* temporary for mock db data services for the front end */
import { InMemoryWebApiModule} from 'angular-in-memory-web-api';
import { InMemoryDataService} from './services/in-memory-data-service';


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { ListContainerComponent } from './modules/list-container/list-container.component';
import {PitchService} from './services/pitch-service';
import { ItemComponent } from './modules/list-container/item/item.component';
import { SearchPanelComponent } from './modules/search/search-panel/search-panel.component';
import {AngularFontAwesomeModule} from "angular-font-awesome";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule, Routes } from "@angular/router";
import { VenueComponent } from './modules/venue-details/venue/venue.component';
import { MapsComponent } from './modules/common/google/maps/maps.component';
import {VenueReservationService} from "./services/cart/venue-reservation.service";
import { VenueReservationComponent } from './modules/common/cart/venue-reservation/venue-reservation.component';

const routes: Routes = [
  {path: '', component: ListContainerComponent},
  {path: ':city',
    children: [
      {path: 'venue/:venueId/:sportId', component: VenueComponent},
      {path: '', redirectTo: '', pathMatch: 'prefix'}
    ]
  },
  {path: '**', redirectTo: '/'}
];

@NgModule({
  declarations: [
    AppComponent,
    ListContainerComponent,
    ItemComponent,
    SearchPanelComponent,
    VenueComponent,
    MapsComponent,
    VenueReservationComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    FormsModule,
    NgbModule.forRoot(),
    // InMemoryWebApiModule.forRoot(InMemoryDataService, { passThruUnknownUrl: true }), //  dataEncapsulation: true
    RouterModule.forRoot(routes, {useHash: false}),
    ReactiveFormsModule,
  ],
  providers: [PitchService, VenueReservationService],
  bootstrap: [AppComponent],
  entryComponents: [VenueReservationComponent]
})
export class AppModule { }
