import { Component, OnInit } from '@angular/core';
import {PitchService} from "../../services/pitch-service";
import {of} from "rxjs/observable/of";
import {PitchesModel} from "./model/pitches-model";
import {PitchData} from "../common/models/pitch-data";

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss']
})
export class ListContainerComponent implements OnInit {
  pitches: PitchData[] | null;
  links: any;
  meta: any;
  isResultsEmpty: boolean = false;
  isLoadingContent: boolean = false;
  isMessageError: boolean = false;
  isMessage: string = '';

  constructor(private pitchDataService: PitchService) {}

  ngOnInit() {
    if (this.pitches) {
      this.isResultsEmpty = false;
      return of(this.pitches);
    }
    this.recieveChangeListNotifcation();
    this.errorMessageNotifications();
    this.isLoadingContent = true;
    this.pitchDataService.getAllPitches().subscribe(allPitches => {
      this.isResultsEmpty = false;
      this.isLoadingContent = false;
      this.pitches = allPitches.data;
      this.links = allPitches.links;
      this.meta = allPitches.meta;
    });
  }

  recieveChangeListNotifcation() {
    this.isResultsEmpty = false;
    this.isLoadingContent = true;
    this.pitchDataService.listAllPitchesChanges$.subscribe(
      (newListOfPitches: PitchesModel) => {
        this.isLoadingContent = false;
        this.pitches = newListOfPitches.data;
        this.links = newListOfPitches.links;
        this.meta = newListOfPitches.meta;
        if (this.pitches.length === 0) {
          console.log('returned no results');
          this.isResultsEmpty = true;
        } else {
          // this.isMessageError = false;
        }
      }, (error) => console.log('error on notification:', error.message));
  }

  errorMessageNotifications() {
    this.pitchDataService.errorMessageChanges$.subscribe(
      (message: string) => {
        this.isMessage = message;
        this.isMessageError = true;
      }, (error) => console.log('error on notification:', error.message));
  }
}
