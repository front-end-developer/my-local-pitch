/**
 * Created by Mark Webley on 20/04/2019.
 */
import {PitchData} from '../../common/models/pitch-data';
import {PitchMetadata} from '../../common/models/pitch-metadata';

export class PitchesModel {
  public meta: PitchMetadata;
  public links: object;
  public data: PitchData[];
  public included: object;

  constructor() {}
}
