import {ItemLinks} from "./item-links";

/**
 * Created by Mark Webley on 16/04/2019.
 */
export class ItemAttributes {
  public name: string;
  public status: string;
  public images: any;
  public sport: string;
  public format: string;
  public surface: string;
  public facilities: object[];
  public payment: object[];
  public about: string;
  public mlp: boolean;
  public online_booking: boolean;
  public tac_link: string;
  public operator: string;
  public opt_in_third_parties: boolean;
  public featured: boolean;
  public featuredOrder: number;
  public showFFPopup: boolean;
  public available_slots: object;
}
