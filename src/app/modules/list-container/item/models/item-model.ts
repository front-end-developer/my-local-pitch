import {ItemLinks} from "./item-links";
import {ItemAttributes} from "./item-attributes";

export class ItemModel {
  public id: number;
  public attributes: ItemAttributes;
  public relationships: object;
  public links: ItemLinks;

  constructor() {
    this.attributes = new ItemAttributes();
    this.links = new ItemLinks();
  }

  /** TODO **/
  name(): string {
    return this.attributes.name;
  }

  /** TODO **/
  about(): string {
    return this.attributes.about;
  }

  /** TODO **/
  images(): object {
    return this.attributes.images;
  }

  /** TODO **/
  payment(): any {
    return this.attributes.payment;
  }

  /** TODO **/
  sport(): string {
    return this.attributes.sport;
  }

  /** TODO **/
  status(): string {
    return this.attributes.status;
  }

  /** TODO **/
  surface(): string {
    return this.attributes.surface;
  }
}
