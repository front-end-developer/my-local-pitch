import {Component, Input, OnInit} from '@angular/core';
import {ItemModel} from "./models/item-model";
import {VenueReservationComponent} from "../../common/cart/venue-reservation/venue-reservation.component";
import {BookVenue} from "../../common/utils/BookVenue";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'list-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent extends BookVenue implements OnInit {

  @Input() model: ItemModel;
  image: string;
  maxWords: number = 12;
  minDescription: string = '';
  showMore: boolean = false;
  webLink: string;

  constructor(private _modalService: NgbModal) {
    super();
  }

  ngOnInit() {
    const test = this.model || 'No data available';
    this.image = this.model.attributes.images.small[0];
    this.webLink = this.model.links.weblink;
    if (this.model.attributes.about !== null) {
      const splitWords = this.model.attributes.about.split(' ');
      this.showMore = splitWords.length >  this.maxWords ? true : false;
      this.minDescription = splitWords.slice(0, this.maxWords).join(' ');
    }
  }

  bookVenue(evt) {
    evt.stopPropagation();
    super.openReservation(this._modalService, VenueReservationComponent);
    return false;
  }
}
