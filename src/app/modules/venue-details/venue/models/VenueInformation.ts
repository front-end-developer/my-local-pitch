import {ItemLinks} from '../../../list-container/item/models/item-links';
import {VenueAttributes} from '../../../common/models/venue-attributes';

/**
 * Created by Mark Webley on 19/04/2019.
 */


export class VenueInformation {
  public id: number;
  public type: string;
  public attributes: VenueAttributes;
  public relationships: object;
  public links: ItemLinks;

  constructor() {
    this.attributes = new VenueAttributes();
    this.links = new ItemLinks();
  }
}
