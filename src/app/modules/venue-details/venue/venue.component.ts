import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PitchService} from '../../../services/pitch-service';
import {VenueInformation} from './models/VenueInformation';
import {ActivatedRoute} from '@angular/router';
import {DateString} from '../../common/utils/DateString';
import {MapsComponent} from "../../common/google/maps/maps.component";
import {VenueReservationComponent} from "../../common/cart/venue-reservation/venue-reservation.component";
import {BookVenue} from "../../common/utils/BookVenue";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: 'app-venue',
  templateUrl: './venue.component.html',
  styleUrls: ['./venue.component.scss']
})
export class VenueComponent extends BookVenue implements OnInit, AfterViewInit {
  venueInformation: VenueInformation;
  includedInformation: PitchService[];
  venueAvailablity: PitchService[];
  isLoadingContent: boolean = false;
  surface: string = '';
  name: string = '';
  address1: string = '';
  address2: string = '';
  area: string = '';
  contact_number: string = '';
  council: string = '';
  postcode: string = '';
  town: string = '';
  nearest_tube: string = '';
  parking_type: string = '';
  mapsComponent: MapsComponent;
  constructor(private pitchDataService: PitchService,
              private route: ActivatedRoute,
              private _modalService: NgbModal) {
    super();
  }

  ngOnInit() {
    let venueId: number = 0;
    const dateToday = DateString.getTodaysDate();
    this.route.params.subscribe(params => {
      const sportId = params['sportId'];
      const startInd = sportId.lastIndexOf('-');
      venueId = sportId.substring(startInd+1);
    });

    if (venueId === 0) {
      console.log('no venue selected');
      return;
    }
    this.isLoadingContent = true;
    this.pitchDataService.getInformation(venueId).subscribe(information => {
      this.name  = information.included[0].attributes.name;
      this.address1 = information.included[0].attributes.address1;
      this.address2 = information.included[0].attributes.address2;
      this.area = information.included[0].attributes.area;
      this.contact_number = information.included[0].attributes.contact_number;
      this.council = information.included[0].attributes.council;
      this.postcode = information.included[0].attributes.postcode;
      this.town = information.included[0].attributes.town;
      this.nearest_tube  = information.included[0].attributes.nearest_tube;
      this.parking_type = information.included[0].attributes.parking_type;
      // this.mapsComponent.updateMap(information.included[0].attributes.longitude, information.included[0].attributes.latitude);

      this.isLoadingContent = false;
      this.venueInformation = information.data;
      this.includedInformation = information.included;
      if (this.venueInformation.attributes.surface !== 'n/a') {
        this.surface = `| ${this.venueInformation.attributes.surface}`;
      }
    });

    // TODO:set dynamic dates
    this.pitchDataService.getAvailability(venueId, '09-01-2018' ).subscribe(availability => {
      this.venueAvailablity = availability;
    });
  }

  ngAfterViewInit() {
   // this.mapsComponent = new MapsComponent();
    this.mapsComponent.setUp();
  }

  bookVenue(evt) {
    evt.stopPropagation();
    super.openReservation(this._modalService, VenueReservationComponent);
    return false;
  }
/*
  bookVenue(evt) {
    evt.stopPropagation();
    this._modalService.open(VenueReservationComponent);
    return false;
  }
  */
}
