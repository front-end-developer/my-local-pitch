import {Component, OnInit, ViewChild} from '@angular/core';
import {PitchService} from "../../../services/pitch-service";
import {Sports} from "./model/sports";
import {of} from "rxjs/observable/of";

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.scss']
})
export class SearchPanelComponent implements OnInit {
  @ViewChild('searchComponent') form: any
  sports: Sports[] | null;
  selectedSport: string = 'select';
  keywords: string = '';
  selectError: string = '';
  formError: string = '';

  constructor(private pitchDataService: PitchService) { }

  ngOnInit() {
    this.getSports();
  }

  getSports() {
    if (this.sports) {
      return of(this.sports);
    }
    this.pitchDataService.getSports().subscribe(sports => {
      this.sports = sports.data;
    });
  }

  changeSelector() {
    this.selectError = '';
  }

  searchVenues() {
    this.formError = '';
    this.selectError = '';
    if (this.selectedSport === 'select') {
      this.selectError = 'error';
      console.log('select sport');
      return;
    }

    if (this.keywords.length <= 3) {
      this.formError = 'error';
      console.log('insert a venue: eg London');
      return;
    }
    this.pitchDataService.getAllPitches(this.selectedSport, this.keywords).subscribe(allPitches => {
      this.pitchDataService.broadcastAllPitchChangeNotification();
    });
  }
}
