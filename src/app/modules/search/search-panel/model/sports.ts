/**
 * Created by Mark Webley on 19/04/2019.
 */
import {SportAttributes} from './sport-attributes';


export class Sports {
  public id: string;
  public type: string;
  public attributes: SportAttributes;

  constructor() {
    this.attributes = new SportAttributes();
  }
}
