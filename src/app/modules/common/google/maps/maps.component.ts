import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
// import {Maps} from "./model/Maps";

declare var google: any;
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit, AfterViewInit {

  // google maps API
  uluru: object = {
    lat: -25.363,
    lng: 131.044
  };

  // map: Maps = new Maps();
  map: any = {
    getZoom() {}
  };
  marker: object;
  zoom: number = 13;

  constructor() { }

  ngOnInit() {}

  @ViewChild('map') mapRef: ElementRef;
  ngAfterViewInit() {
    this.setUp();
  }

  public setUp() {
    setTimeout(() => {
      this.map = new google.maps.Map(this.mapRef.nativeElement, {
        zoom: this.zoom,
        center: this.uluru
      });
      this.marker = new google.maps.Marker({
        position: this.uluru,
        map: this.map
      });

    }, 2000)
  }

  public reset() {
    this.uluru = {
      lat: -25.363,
      lng: 131.044
    };
    this.map = new google.maps.Map(this.mapRef.nativeElement, {
      zoom: this.zoom,
      center: this.uluru
    });
    this.marker = new google.maps.Marker({
      position: this.uluru,
      map: this.map
    });
  }
  public updateMap(lats: number, lon: number) {
    const domElementForMap = this.mapRef.nativeElement;
    if (typeof domElementForMap === 'undefined') {
      const trackMapInUI = setInterval(() => {
        if ( domElementForMap != 'undefined') {
          this.updateMap(lats, lon);
          clearInterval(trackMapInUI);
        }
      }, 1000);
      return;
    }
    this.zoom = this.map.getZoom();
    this.uluru = { lat: lats, lng: lon };
    this.map = new google.maps.Map(this.mapRef.nativeElement, {
      zoom: this.zoom,
      center: this.uluru
    });
    this.marker = new google.maps.Marker({
      position: this.uluru,
      map: this.map
    });
  }

}
