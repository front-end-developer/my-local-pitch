/**
 * Created by Mark Webley on 20/04/2019.
 */
export class PitchMetadata {
  total_pages: number;
  total_items: number;
  on_page_online_venues: number;
  total_online_venues: number;
  aggregations: object;
  available_filters: object;
}
