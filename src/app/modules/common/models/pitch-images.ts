/**
 * Created by Mark Webley on 20/04/2019.
 */
export class PitchImages {
  small: string[];
  medium: string[];
  large: string[];
}
