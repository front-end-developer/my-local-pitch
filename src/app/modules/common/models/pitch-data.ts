/**
 * Created by Mark Webley on 20/04/2019.
 */
import {PitchImages} from '../../common/models/pitch-images';
import {PitchLinks} from '../../common/models/pitch-links';
import {VenueAttributes} from './venue-attributes';

export class PitchData  {
  public id: number;
  public type: string;
  public attributes: VenueAttributes;
  public relationships: object;
  public: PitchLinks;
}
