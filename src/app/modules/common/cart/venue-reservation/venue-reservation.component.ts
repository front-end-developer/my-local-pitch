import {Component, OnInit, ViewChild} from '@angular/core';
import {VenueReservation} from "../models/venue-reservation";
import {VenueReservationService} from "../../../../services/cart/venue-reservation.service";
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ValidationMessages} from "../models/validation-message";
import {FormErrors} from "../models/form-errors";

@Component({
  selector: 'app-venue-reservation',
  templateUrl: './venue-reservation.component.html',
  styleUrls: ['./venue-reservation.component.scss']
})
export class VenueReservationComponent implements OnInit {

  private static cart: VenueReservation;
  bookingFrom: FormGroup;
  date: string;
  dateStart: string;
  dateEnd: string;
  errorInForm: boolean = true;
  validationMessages = new ValidationMessages().messages;
  formErrors = new FormErrors().formErrors;

  constructor(private venueReservationService: VenueReservationService,
              public modal: NgbActiveModal,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.bookingFrom = this.fb.group({
      fullName: ['', [
        Validators.required,
        Validators.minLength(5)]
      ],
      email:  ['', [
        Validators.required,
        Validators.minLength(5)]
      ],
      telephone:  ['', [
        Validators.required,
        Validators.minLength(7)]
      ],
      message:  [''],
      dateStart: ['', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)]
      ],
      dateEnd: ['', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)]
      ]
    })
  }

  validateForm(group: FormGroup) {
    this.errorInForm = false;
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.validateForm(abstractControl);
      } else {
        this.formErrors[key] = '';
        if (abstractControl && !abstractControl.valid) {
          const messages = this.validationMessages[key];
          for (const errorKey in abstractControl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + ' ';
              this.errorInForm = true;
            }
          }
        }
      }
    });
  }

  bookReservation() {
    this.validateForm(this.bookingFrom);
    if (!this.errorInForm) {
      this.modal.close('Ok click');
      this.addToCart();
    }
  }

  cancel() {
    this.modal.dismiss('cancel click')
  }

  /**
   * TODO:
   * finish off
   * store data into cart and send out notification
   */
  addToCart() {
    // this.cart({reactive forms data....})
    // this.updateCartNotification(this.cart)
  }

  /**
   * TODO:
   * finish off
   * when you update cart, send to change notifications for listeners
   */
  updateCartNotification() {
    this.venueReservationService.broadcastVenueReservationChangeNotification(VenueReservationComponent.cart);
  }

}
