/**
 * Created by Mark Webley on 22/04/2019.
 */
export class ValidationMessages {
  messages = {
    'fullName': {
      'required': 'Full name is required',
      'minlength': 'Please enter your full name',
    },
    'email': {
      'required': 'Email is required',
      'minlength': 'Please enter a valid email',
    },
    'telephone': {
      'required': 'Telephone is required',
      'minlength': 'Please enter a valid telephone number',
    },
    'message': {
      'required': 'Please enter a valid message',
    },
    'dateStart': {
      'required': 'Start date is required to book this venue',
      'minlength': 'Please enter venue start date',
      'maxlength': 'Please enter valid start date',
    },
    'dateEnd': {
      'required': 'End date is required to book this venue',
      'minlength': 'Please enter venue end date',
      'maxlength': 'Please enter valid end date',
    }
  }
}
