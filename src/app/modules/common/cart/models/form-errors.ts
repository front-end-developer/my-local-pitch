/**
 * Created by Mark Webley on 22/04/2019.
 */
export class FormErrors {
  formErrors = {
    'fullName': '',
    'email': '',
    'telephone': '',
    'message': '',
    'dateStart': '',
    'dateEnd': ''
  }
}
