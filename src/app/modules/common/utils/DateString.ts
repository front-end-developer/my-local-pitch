/**
 * Created by Mark Webley on 19/04/2019.
 */
export class DateString {

  public static getTodaysDate(){
    const fromToday = new Date();
    const month = String(fromToday.getMonth()).length === 1 ? `0${fromToday.getMonth()}` : fromToday.getMonth();
    const dayDate = String(fromToday.getDate()).length === 1 ? `0${fromToday.getDate()}` : fromToday.getDate();
    return `${fromToday.getFullYear()}-${month}-${dayDate}`;
  }
}
