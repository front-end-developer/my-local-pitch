import {VenueReservationComponent} from "../cart/venue-reservation/venue-reservation.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

/**
 * Created by Mark Webley on 22/04/2019.
 */
export class BookVenue {
  constructor() {}

  openReservation(_modalService: NgbModal, modalComponent: any, evt?: any): void {
    if (evt) {
      evt.stopPropagation();
    }
    _modalService.open(modalComponent, {
      backdrop: 'static',
      keyboard: false
    });
  }
}
