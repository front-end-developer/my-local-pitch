# Example of a test in Angular 5++, Rest services, bootstrap 4, reactive forms, validation, geoLocation api, calenders etc
* Author: Mark Webley
* Author mail: no spam emails :)
* Author contact spanish mobile: 0034 631 33 61 33 | uk whatsApp 0044 7930 743 923 
* Entreprenuer: I build with business in mind, to generate opportunities into income. Ecommerce funnel all the way through to payment.

## Test URls in your local

http://localhost:portnumber/

and 

http://localhost:portnumber/london/venue/drayton-green-running-track/athletics-outdoor-33192

## API KEY 

Set the API KEY in GET-THE-KEY-FROM-YOUR-GOOGLE-ACCOUNT.

## Services 

To use In Memory Data Base instead of the live services in your local environment uncomment line InMemoryWebApiModule in app.module.ts, and switch the PITCHES endpoint in environment.ts.
When using inMemoryWebApiModule, the stubs for the mock services are inside services/stubs. 

## Common classes and modules 

Shared modules, models, and utilities inside, modules/common, modules/common/models

## Build

Run `ng build` to build the project. 
Run `ng serve` to run in your dev environment.
The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Search panel functionality - debounce etc

Will finish when I get time, I will update this to use debounce etc.

## Use typeahead in search panel functionality

May do that if I get time, because this is only a test.

## Running unit tests: NOT DONE

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Running end-to-end tests: NOT DONE

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## load data and limit by on the first page: NOT DONE
I might do it when I get time, because this is just a test


## Front End Angular - first draft - responsive web booking solution

![Alt text](screenshots/reactive-forms-etc.png "Front End Angular - first draft")

### Calendar:
![Alt text](screenshots/reactive-forms-2-etc.png "Front End Angular - first draft")


## Front End Angular - first draft - responsive web desktop

![Alt text](screenshots/responsive-desktop.png "Front End Angular - first draft")


## Front End - first draft - responsive web mobile

![Alt text](screenshots/responsive-mobile.png "Front End - first draft")



## Detail Module - with data coming in from services first draft just data, with google geolocation API
![Alt text](screenshots/detail-page-from-services.png "Detail Module - with data coming in from services first draft just data")


## Detail Module - mobile responsive with google geolocation API
![Alt text](screenshots/detail-page-from-services-mobile-responsive.png "Detail Module - with data coming in from services first draft just data")


